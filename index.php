<!DOCTYPE html>
<html lang="en"><head>
<meta charset="utf-8">
<title>JavaScript Form Validation using a sample registration form</title>
<meta name="keywords" content="example, JavaScript Form Validation, Sample registration form" />
<meta name="description" content="This document is an example of JavaScript Form Validation using a sample registration form. " />
<link rel='stylesheet' href='js-form-validation.css' type='text/css' />
<script src="sample-registration-form-validation.js"></script>
</head>
<body onload="document.registration.userid.focus();">
<h3 align="right" style=margin-right:20px;"> <a href="login.php">Admin login</a></h3>
<h1 align="center">Registration Form</h1>

Use tab keys to move from one input field to the next.
 <form  id="register_form" >
	<table width="98%" border="0" cellspacing="5" cellpadding="5">
	  <tr>
		<td>Username</td>
		<td>
		<input type="text" name="Userid">
		
		</td>
	  </tr>
	  <tr>
		<td>Email </td>
		<td>
		<input type="text" name="email">
		</td>
	  </tr>
	  <tr>
		<td>Password</td>
		<td>
		<input type="password" name="password">
		</td>
	  </tr>
	  <tr>
		<td>Confirm Password</td>
		<td>
		<input type="password" name="password">
		</td>
	  </tr>
	  <tr>
		<td>Upload photo</td>
		<td>
		<input type="enctype" name="address">
		</td>
	  </tr>
	  
	   
	  
	   
	  
	  <tr>
		<td>  </td>
		<td><input id="submit" type="submit" name="submit" value="Submit" /></td>
	  </tr>
	</table>
	
	
	 </form>
 

</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
   
   
    <script type="text/javascript">
    $(document).ready(function() {
    
   
     
		$('#submit').click(function () 
		{         
		  
			var name = $('input[name=name]').val();
			var email = $('input[name=email]').val();
			var password = $('input[name=password]').val();
			var contact = $('input[name=contact]').val();
			var address = $('input[name=address]').val();
			 
	 
			
			//organize the data properly
			var form_data = 
			  'name='+name+
			  '&email='+email+
			  '&password='+password+
			  '&contact='+contact+
			  '&address='+address;
			 
		
			 
			 
			//start the ajax
			$.ajax({
				//this is the php file that processes the data and send mail
				url: "ajax/process.php",
				 
				//POST method is used
				type: "POST",
	 
				//pass the data        
				data: form_data,    
				 
				
				//success
				success: function (html) {             
					//if process.php returned 1/true (send mail success)
					if (html==1) {                 
						//hide the form
						$('#register_form').fadeOut('slow');                
						 
						 
						 //hide the loader
						 $('.loading').fadeOut();   
						 
						//show the success message
						$('.message').html('Successfully Registered ! ').fadeIn('slow');
						 
						 
						 
					//if process.php returned 0/false
					} else alert('Sorry, unexpected error. Please try again later.');              
				}      
			});
			 
			//cancel the submit button default behaviours
			return false;
    });
}); 
 
 
 </script>